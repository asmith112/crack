#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[]){
    // Check to see how many args there are.
    // If only 1, then no filename was supplied.
    
    if (argc < 3){
        
        printf("You must supply a filename and a destination\n");
        exit(1);
    }
    
    //check to see if source and destination are the same name if they are then exit program
    int chkname = strcmp(argv[1],argv[2]);
    if(chkname == 0){
        printf("Source and Destination may not be the same\n");
        exit(1);
    }
    
    // Check to see if the file is actually a directory
    struct stat filestat;
    stat(argv[1], &filestat);
    if ((filestat.st_mode & S_IFMT) == S_IFDIR){
        
        printf("%s is a directory\n", argv[1]);
        exit(1);
    }
    
    // Open the file for reading
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (fp == NULL){
        
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    
    
    FILE *fp1;
    fp1 = fopen(argv[2],"w");
    
    // Loop through file, reading up to 100 chars at a time.
    char line[100];
    char* strings[101];
    char *meh[101];
    int i = 0;
    int num = 0;
    while(fgets(line, 100, fp) != NULL){
        int len = strlen(line)-1;
        //printf("%s",line);
        strings[i] = strdup(line);
        meh[i] = md5(line, len);
        i++;
        num++;

        //printf("%d",len);
    }
     for (int j=0 ; j<num; j++) {
        printf("%s", strings[j]);
    }
    for (int j=0 ; j<num; j++) {
        printf("\n%s", meh[j]);
        fputs(meh[j],fp1);
        fprintf(fp1,"\n");
    }

        
    
    
    // Close the file when done.
    fclose(fp);
    fclose(fp1);
 
}